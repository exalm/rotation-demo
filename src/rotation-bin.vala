public enum RotationTest.Rotation {
    NORMAL,
    90DEG,
    180DEG,
    270DEG;

    public float get_angle () {
        switch (this) {
            case NORMAL:
                return 0;
            case 90DEG:
                return 90;
            case 180DEG:
                return 180;
            case 270DEG:
                return 270;
            default:
                assert_not_reached ();
        }
    }

    public static Rotation from_angle (double angle) {
        angle = normalize (angle);

        if (angle < 0)
            angle += 360;

        if (angle < 45)
            return NORMAL;

        if (angle < 135)
            return 90DEG;

        if (angle < 225)
            return 180DEG;

        if (angle < 315)
            return 270DEG;

        return NORMAL;
   }
}

double deg2rad (double deg) {
    return deg * Math.PI / 180;
}

double rad2deg (double rad) {
    return rad * 180 / Math.PI;
}

double normalize (double angle) {
    angle = angle % 360;

    if (angle < 0)
        angle += 360;

    return angle;
}

public class RotationTest.RotationBin : Gtk.Widget, Gtk.Buildable {
    private Gtk.Widget? _child;
    public Gtk.Widget? child {
        get { return _child; }
        set {
            if (child == value)
                return;

            if (child != null)
                child.unparent ();

            _child = value;

            if (child != null)
                child.set_parent (this);
        }
    }

    private Rotation _rotation;
    public Rotation rotation {
        get { return _rotation; }
        set {
            _rotation = value;

            if (animation != null)
                animation.stop ();

            double target = rotation.get_angle ();
            
            if (target - progress > 180)
                target -= 360;
            else if (target - progress < -180)
                target += 360;

            animation = new SpringAnimation.with_damping_ratio (
                this,
                progress,
                target,
                0,
                1,     // ratio
                1,     // mass
                300,   // stiffness,
                0.0001 // epsilon
            );
            animation.notify["value"].connect (() => {
                progress = (float) normalize (animation.value);
                queue_resize ();
            });
            animation.done.connect (() => {
                animation = null;
                rotating = false;
            });

            rotating = true;
            animation.start ();
        }
    }

    private float progress = 0f;
    private double initial_angle;
    private bool rotating;
    private SpringAnimation? animation;

    construct {
        var gesture = new Gtk.GestureRotate ();
        gesture.begin.connect (() => {
            initial_angle = progress;

            if (animation != null)
                animation.stop ();

            rotating = true;

            gesture.set_state (CLAIMED);
        });
        gesture.angle_changed.connect ((angle, angle_delta) => {
            progress = (float) normalize (initial_angle + rad2deg (angle_delta));
            queue_resize ();
        });
        gesture.end.connect (() => {
            rotation = Rotation.from_angle (progress);

            gesture.reset ();
        });
        gesture.cancel.connect (() => {
            rotation = Rotation.from_angle (progress);
        });

        add_controller (gesture);
    }

    static construct {
        set_css_name ("rotationbin");
        set_accessible_role (GROUP);

        install_action ("rotation.left", null, widget => {
            var self = widget as RotationBin;

            double angle = normalize (self.rotation.get_angle () - 90);
            self.rotation = Rotation.from_angle (angle);
        });

        install_action ("rotation.right", null, widget => {
            var self = widget as RotationBin;

            double angle = normalize (self.rotation.get_angle () + 90);
            self.rotation = Rotation.from_angle (angle);
        });
    }

    protected override void dispose () {
        child = null;

        base.dispose ();
    }

    protected void add_child (Gtk.Builder builder, Object object, string? type) {
        if (object is Gtk.Widget)
            child = object as Gtk.Widget;
        else
            base.add_child (builder, object, type);
    }

    protected override void measure (Gtk.Orientation orientation, int for_size, out int min, out int nat, out int min_baseline, out int nat_baseline) {
        if (child == null) {
            min = nat = 0;
            min_baseline = nat_baseline = -1;
            return;
        }

        Gtk.Orientation opposite_orientation;
        if (orientation == HORIZONTAL)
            opposite_orientation = Gtk.Orientation.VERTICAL;
        else
            opposite_orientation = Gtk.Orientation.HORIZONTAL;

        if (rotating) {
            int min1, nat1, min_baseline1, nat_baseline1;
            int min2, nat2, min_baseline2, nat_baseline2;

            child.measure (orientation, for_size,    out min1, out nat1, out min_baseline1, out nat_baseline1);
            child.measure (opposite_orientation, -1, out min2, out nat2, out min_baseline2, out nat_baseline2);

            double t = Math.sin (deg2rad (progress)).abs ();

            min = (int) Math.ceil (lerp (min1, min2, t));
            nat = (int) Math.ceil (lerp (nat1, nat2, t));

            if (min_baseline1 >= 0 && min_baseline2 >= 0)
                min_baseline = (int) Math.ceil (lerp (min_baseline1, min_baseline2, t));
            else
                min_baseline = -1;

            if (nat_baseline1 >= 0 && nat_baseline2 >= 0)
                nat_baseline = (int) Math.ceil (lerp (nat_baseline1, nat_baseline2, t));
            else
                nat_baseline = -1;
        } else {
            if (rotation == 90DEG || rotation == 270DEG)
                orientation = opposite_orientation;

            child.measure (orientation, for_size, out min, out nat, out min_baseline, out nat_baseline);
        }
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (child == null)
            return;

        int w, h;
        if (rotating) {
            double a = deg2rad (progress % 180) / Math.PI * 2;
            double t = a < 1 ? a : 2 - a; // Math.sin (deg2rad (progress)).abs ();

            w = (int) Math.ceil (lerp (width, height, t));
            h = (int) Math.ceil (lerp (height, width, t));
        } else {
            bool sideways = rotation == 90DEG || rotation == 270DEG;
            
            w = sideways ? height : width;
            h = sideways ? width : height;
        }

        var transform = new Gsk.Transform ();
        transform = transform.translate ({ width / 2f, height / 2f });
        transform = transform.rotate (progress);
        transform = transform.translate ({ -w / 2f, -h / 2f });

        child.allocate (w, h, baseline, transform);
    }
}
